from django.test import TestCase, Client
import unittest
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index
import time

# Create your tests here.

class Story9(TestCase):

    def test_page_login(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)
    
    def test_page_logout(self):
        response = Client().get('/story9/logout')
        self.assertEqual(response.status_code,301)
    
    def test_page_template_logout(self):
        response = Client().get('/story9/logout')
        self.assertEqual(response.status_code,301)

    def test_page_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response,'story9.html')
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello!",html_response)

class FuncTest9(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/story9')
    
    def tearDown(self):
        self.selenium.quit()
    
    def test_open_page(self):
        browser = self.selenium
        self.assertIn("Go the Book",browser.title)
    
    def test_header_page_before_login(self):
        browser = self.selenium
        self.assertIn("Hello!",browser.page_source)
    
    def test_header_page_after_login(self):
        browser = self.selenium
        browser.find_element_by_xpath('//*[@id="id_username"]').send_keys("Tester")
        browser.find_element_by_xpath('//*[@id="id_password"]').send_keys("akucintangoding123")
        browser.find_element_by_xpath('//*[@id="submit"]').click()
        time.sleep(5)
        self.assertIn("Have a nice day!",browser.page_source)
        self.assertIn("Tester",browser.page_source)
        browser.find_element_by_xpath('/html/body/div[2]/a').click()
        time.sleep(5)
        self.assertIn("Hello!",browser.page_source)



