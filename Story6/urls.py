from django.urls import path
from .views import index


appname = 'Story6'

urlpatterns = [
   path('', index, name = 'homepage'),
]