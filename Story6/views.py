from django.shortcuts import render
from .models import Status
from .forms import StatusForm
from datetime import datetime

# Create your views here.
def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            status = Status()
            status.status = form.cleaned_data['status']
            status.time = datetime.now()
            status.save()
    status = Status.objects.all()
    form = StatusForm()
    response = {"status":status, 'form' : form}
    return render(request,'story6.html',response)