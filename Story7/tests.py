from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options

# # Create your tests here.

class Story7(TestCase):

    def test_page(self):
        response = Client().get("/story7")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get("/story7")
        self.assertTemplateUsed(response,'story7.html')
    
    def test_greetings(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hi! I'm Gibran Brahmanta",html_response)

class FuncTest7(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/story7')
    
    def tearDown(self):
        self.selenium.quit()

    def test_mode(self):
        browser = self.selenium
        body_background = browser.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("rgba(0, 0, 0, 0) linear-gradient(rgb(2, 0, 36) 0%, rgb(9, 9, 121) 35%, rgb(0, 212, 255) 100%)", body_background)
        switch_button = browser.find_element_by_class_name("switch")
        switch_button.click()
        body_background = browser.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("rgb(245, 208, 32) linear-gradient(0deg, rgb(245, 208, 32) 0%, rgb(245, 56, 3) 74%)", body_background)
    
    def test_accordion_1(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-1"]')
        accordion.click()
        self.assertIn("Prog. Foundation 1 Teaching Assistant",browser.page_source)
    
    def test_accordion_2(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-3"]')
        accordion.click()
        self.assertIn("Staff of Research and Strategic Action Departement, BEM Fasilkom UI",browser.page_source)
    
    def test_accordion_3(self):
        browser = self.selenium
        accordion = browser.find_element_by_xpath('//*[@id="ui-id-5"]')
        accordion.click()
        self.assertIn("Currently searching for some awards",browser.page_source)

    
    