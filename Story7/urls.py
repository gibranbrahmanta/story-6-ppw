from django.urls import path
from .views import index


appname = 'Story7'

urlpatterns = [
   path('', index, name = 'accordion'),
]