from django.urls import path
from .views import index,data


appname = 'Story8'

urlpatterns = [
   path('', index, name = 'bookList'),
   path('data/', data, name='books_data'),
]